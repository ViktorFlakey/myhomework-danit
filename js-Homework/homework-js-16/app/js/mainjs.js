let btnThems = document.getElementById('btn');
btnThems.addEventListener('click', changeTheme);
let link = document.getElementById('page-theme');
let css = link.getAttribute('href');
css = localStorage.getItem("theme");
link.setAttribute("href", css);
localStorage.setItem('theme', css);

function changeTheme() {
    const theme = localStorage.getItem("theme");
    if(theme === "app/css/dark.css") {
        localStorage.setItem("theme", "app/css/light.css");
    }
    else {
        localStorage.setItem("theme", "app/css/dark.css");
    }
    location.reload();

}
