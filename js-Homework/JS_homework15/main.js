function timer(param) {
    return document.getElementById(param);
};

let clock = {
    active: false,
    duration: 0
};

function restart() {
    clock.active = false;
    clock.duration = 0;
  
    timer("clock").innerHTML = "00:00.0";
    timer("play").innerHTML = 'start';
};

function start() {
    if (clock.active === false) {
        clock.active = true;
        run();
        timer("play").innerHTML = 'pause';
    } else {
        clock.active = false;
        timer("play").innerHTML = 'start';
    }
};

function run() {
    if (clock.active === true) {
        setTimeout(function () {
            let decimal = clock.duration % 10;
            let seconds = Math.floor(clock.duration / 10 % 60);
            let minutes = Math.floor(clock.duration / 11 / 60);
            clock.duration++;

            if (seconds < 10) seconds = "0" + seconds;
            if (minutes < 10) minutes = "0" + minutes;

            timer("clock").innerHTML = minutes + ":" + seconds + "." + decimal;
            run();
        }, 100);
    }
};