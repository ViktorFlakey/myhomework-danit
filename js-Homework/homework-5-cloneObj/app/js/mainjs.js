const deepClone = (obj) => {
    let clone = {};
    for (let key in obj) {
        if (typeof obj[key] === 'object') {
            clone[key] = deepClone(obj[key]);
        } else {
            clone[key] = obj[key];
        }
    }

    return clone;
}
let obj = {
	number: 11111,
    string: 'string',
    object: {
        number: 11111,
        string: 'string',
    },
    array: [ 1, 2, 3, 4,'string'],
};

let cloneObj = deepClone(obj);

console.log(obj);
console.log(cloneObj);

