$(document).ready(function(){
//  Header section
const arrHeadMenu = Array.prototype.slice.call(document.getElementsByClassName('header-menu-link'));
    let logoHome = document.getElementById('head-logo');
    logoHome.addEventListener('click',function() {
       for(let i = 0; i < arrHeadMenu.length; i++){
                 arrHeadMenu[i].classList.remove('active');                       
                }  
        arrHeadMenu[0].classList.add('active');       
        })
arrHeadMenu[0].classList.add('active'); 
for (let i = 0, arrLeng = arrHeadMenu.length; i < arrLeng; i++) {
            arrHeadMenu[i].addEventListener('click', function () {        
            let clickIndex = arrHeadMenu.indexOf(arrHeadMenu[i]);        
            for(let i = 0; i < arrHeadMenu.length; i++){
                 arrHeadMenu[i].classList.remove('active');                       
                }  
                arrHeadMenu[i].classList.add('active');                
                })  
} 
    
// Service Section
const arrServiceMenu = Array.prototype.slice.call(document.getElementsByClassName('sevice-info-link'));
const serviceContent = Array.prototype.slice.call(document.getElementsByClassName('service-info-description'));
let elemServiseActive = arrServiceMenu[localStorage.getItem('serviceindex')];
let elemServiceVisible =serviceContent[localStorage.getItem('serviceindex')];
if(!localStorage.getItem('serviceindex')) {
    elemServiseActive= arrServiceMenu[0];
    elemServiceVisible = serviceContent[0];
}
elemServiseActive.classList.add('active');
elemServiceVisible.classList.add('active');
    for(let i = 0, arrLen = arrServiceMenu.length; i < arrLen; i++) {          
            arrServiceMenu[i].addEventListener('click', function () {
                let clickIndex = arrServiceMenu.indexOf(arrServiceMenu[i]);  
                localStorage.setItem('serviceindex', clickIndex);               
                for(let i = 0; i < arrServiceMenu.length; i++){
                 arrServiceMenu[i].classList.remove('active');                   
                }  
                arrServiceMenu[i].classList.add('active');
                localStorage.setItem('classActive','active');                
                for(let i = 0; i < serviceContent.length; i++){
				serviceContent[i].classList.remove('active');
			}
			    serviceContent[clickIndex].classList.add('active');
                localStorage.setItem('classVisible','visible');       
            })
    }
//    amazing Work section
const amazingCategBtn = Array.prototype.slice.call(document.getElementsByClassName('amazing-menu-btn'));
amazingCategBtn[0].classList.add('active');
const CategItem = document.querySelectorAll('.amazing-category .amazing-item');

for(let i = 0, arrLen = amazingCategBtn.length; i < arrLen; i++) {       
        amazingCategBtn[i].addEventListener('click', function () {
                let amazingCategItem = document.querySelectorAll('.amazing-category .amazing-item');
                let clickIndex = amazingCategBtn.indexOf(amazingCategBtn[i]);                
                if(clickIndex !== 0)  {          
                    let elemAtribute = amazingCategBtn[i].getAttribute('class').split(' '); 
                    let clasCategory = document.querySelectorAll(`.amazing-category .${elemAtribute[1]}`);
                        for(let i=0; i< amazingCategItem.length;i++) {        
                        amazingCategItem[i].classList.add('novisible');             
                        for(let j = 0; j< clasCategory.length;j++) {                   
                           clasCategory[j].classList.remove('novisible');
                        }              
                    }
                }
                else {
                    for(let j = 0;j<amazingCategItem.length;j++) {
                        amazingCategItem[j].classList.add('novisible');
                        for(let i=0; i <12;i++) {                         
                        CategItem[i].classList.remove('novisible');              
                        }
                    }
                }               
                for(let i = 0; i < amazingCategBtn.length; i++){
                    amazingCategBtn[i].classList.remove('active');                   
                }  
                amazingCategBtn[i].classList.add('active');        
        })    
}  
$(".amazing-item").hover(function(){
	let hoverInfo = $(this).data("info").split(",");
	$(this).animate("slow");
	$(this).prepend(`<div class="amazing-hover-item">
                     <div class="amazing-hover-icon">
	                     <a href=""><i class=" fas fa-link"></i></a> 
	                     <a href=""><i class="search-icon fas fa-search"></i></a>
                    </div>
                    <p class="category-description">${hoverInfo[0]}<span>${hoverInfo[1]}</span></p>
                   </div>`);
    });    
   $('.button-load').click(function() {        
		$('.amazing-category .novisible').delay().show(1400);
       $('.button-load').hide(500);
	});
    
//    section about
    const personsItem =Array.prototype.slice.call(document.getElementsByClassName('person-item')); 
    const personsinfo =Array.prototype.slice.call(document.getElementsByClassName('person-description')); 
    let leftBtn = document.getElementById('left');
    let rightBtn = document.getElementById('right');
    let active = 2;     
    leftBtn.addEventListener('click',function(){
            active--;
            let a = active +1;
            if(active == -1) {
                active = personsItem.length-1;
            }
            for(let i = 0;i<personsItem.length;i++) {
                personsItem[i].classList.remove('active');
                }             
            let personsPhoto = Array.prototype.slice.call(document.querySelectorAll('.person-item img'));
            let personsAtibuteImg = personsPhoto[active].getAttribute('src');
            let photo = document.getElementById('person-img');
            let personsAtibuteData = personsPhoto[active].getAttribute('data-info').split(",");
            let personName = document.getElementById('person-name').innerHTML=`${personsAtibuteData[0]}`;
            let personPosition = document.getElementById('person-position').innerHTML=`${personsAtibuteData[1]}`;          
            photo.setAttribute('src',personsAtibuteImg);
            photo.setAttribute('src',personsAtibuteImg);
            personsItem[active].classList.add('active');
            personsinfo[active].classList.remove('novisible');
            personsinfo[a].classList.add('novisible');            
        })
    rightBtn.addEventListener('click',function(){
        active++;
        let a = active -1;           
            if(active == personsItem.length) {
                active =0;
            }
            for(let i = 0;i<personsItem.length;i++) {
                personsItem[i].classList.remove('active');                
            }
            let personsPhoto = Array.prototype.slice.call(document.querySelectorAll('.person-item img'));
            let personsAtibuteImg = personsPhoto[active].getAttribute('src');
            let personsAtibuteData = personsPhoto[active].getAttribute('data-info').split(",");
            let personName = document.getElementById('person-name').innerHTML=`${personsAtibuteData[0]}`;
            let personPosition = document.getElementById('person-position').innerHTML=`${personsAtibuteData[1]}`;    
            let photo = document.getElementById('person-img');
            photo.setAttribute('src',personsAtibuteImg);
            personsItem[active].classList.add('active');
            personsinfo[active].classList.remove('novisible');
            personsinfo[a].classList.add('novisible');            
        })  
});






 